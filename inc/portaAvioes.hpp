#ifndef PORTAAVIOES_HPP
#define PORTAAVIOES_HPP

#include "../inc/navio.hpp"
#include <string>

using namespace std;

class PortaAvioes : public Navio {
	private:
		string orientacao;
	public:
		PortaAvioes();
		PortaAvioes(int posLinha, int posColuna, string orientacao);
		~PortaAvioes();

		string get_orientacao();
		void set_orientacao(string orientacao);

		bool abater_missel();
};

#endif