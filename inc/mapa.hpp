#ifndef MAPA_HPP
#define MAPA_HPP

#include "../inc/navio.hpp"
#include "../inc/canoa.hpp"
#include "../inc/submarino.hpp"
#include "../inc/portaAvioes.hpp"

#include <bits/stdc++.h>

using namespace std;

class Mapa {
    private:
        int tamanhoMapa;
    public:
        Mapa();
        ~Mapa();
        int get_tamanhoMapa();
        void set_tamanhoMapa(int tamanhoMapa);

        void exibir_mapa();
};

#endif