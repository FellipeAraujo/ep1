#ifndef CANOA_HPP
#define CANOA_HPP

#include "../inc/navio.hpp"

using namespace std;

class Canoa : public Navio {
	public:
		Canoa();
		Canoa(int posLinha, int posColuna);
		~Canoa();
};

#endif