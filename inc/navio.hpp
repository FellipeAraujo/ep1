#ifndef NAVIO_HPP
#define NAVIO_HPP

using namespace std;

class Navio {
	private:
		int posLinha;
		int posColuna;
	public:
		Navio();
		Navio(int posLinha, int posColuna);
		~Navio();

		int get_posLinha();
		void set_posLinha(int posLinha);

		int get_posColuna();
		void set_posColuna(int posColuna);
};

#endif