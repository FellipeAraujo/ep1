#include "../inc/mapa.hpp"
#include "../inc/navio.hpp"
#include "../inc/canoa.hpp"
#include "../inc/submarino.hpp"
#include "../inc/portaAvioes.hpp"

#include <bits/stdc++.h>

Mapa::Mapa(){
    tamanhoMapa = 13;
}

Mapa::~Mapa(){
}

int Mapa::get_tamanhoMapa(){
    return tamanhoMapa;
}

void Mapa::set_tamanhoMapa(int tamanhoMapa){
    this->tamanhoMapa = tamanhoMapa;
}

void Mapa::exibir_mapa(){
	cout << "Mapa do jogo:" << endl << endl;
cout << "    __ __ __ __ __ __ __ __ __ __ __ __ __" << endl;
cout << "00 |__|__|__|__|__|__|__|__|__|__|__|__|__|" << endl;
cout << "01 |__|__|__|__|__|__|__|__|__|__|__|__|__|" << endl;
cout << "02 |__|__|__|__|__|__|__|__|__|__|__|__|__|" << endl;
cout << "03 |__|__|__|__|__|__|__|__|__|__|__|__|__|" << endl;
cout << "04 |__|__|__|__|__|__|__|__|__|__|__|__|__|" << endl;
cout << "05 |__|__|__|__|__|__|__|__|__|__|__|__|__|" << endl;
cout << "06 |__|__|__|__|__|__|__|__|__|__|__|__|__|" << endl;
cout << "07 |__|__|__|__|__|__|__|__|__|__|__|__|__|" << endl;
cout << "08 |__|__|__|__|__|__|__|__|__|__|__|__|__|" << endl;
cout << "09 |__|__|__|__|__|__|__|__|__|__|__|__|__|" << endl;
cout << "10 |__|__|__|__|__|__|__|__|__|__|__|__|__|" << endl;
cout << "11 |__|__|__|__|__|__|__|__|__|__|__|__|__|" << endl;
cout << "12 |__|__|__|__|__|__|__|__|__|__|__|__|__|" << endl;
cout << "    00 01 02 03 04 05 06 07 08 09 10 11 12" << endl;
}