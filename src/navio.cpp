#include "../inc/navio.hpp"
#include <iostream>

Navio::Navio() {
	//cout << "Construtor da classe Navio" << endl;
}

Navio::Navio(int posLinha, int posColuna){
	set_posLinha(posLinha);
	set_posColuna(posColuna);
}

Navio::~Navio() {
	//cout << "Destrutor da classe Navio" << endl;
}

int Navio::get_posLinha(){
	return posLinha;
}
void Navio::set_posLinha(int posLinha){
	this->posLinha = posLinha;
}

int Navio::get_posColuna(){
	return posColuna;
}
void Navio::set_posColuna(int posColuna){
	this->posColuna = posColuna;
}